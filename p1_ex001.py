import random

while True:
    # pede o tamanho do dado que deseja rolar
    tamanho_dado_str = input("Forneça o tamanho do dado que será rolado (ENTER para sair): ")
    if not tamanho_dado_str:
        break
    try:
        tamanho_dado = int(tamanho_dado_str)
        if tamanho_dado <= 0:
            raise ValueError
    except ValueError:
        print("A informação passada não é válida!")
        continue

    # pede o número de dados que deseja rolar
    num_dados_str = input("Forneça o número de dados que serão rolados (em branco == 1): ")
    if not num_dados_str:
        num_dados = 1
    else:
        try:
            num_dados = int(num_dados_str)
            if num_dados <= 0:
                raise ValueError
        except ValueError:
            print("A informação passada não é válida!")
            continue

    # gera valores aleatórios para cada dado e imprime-os
    resultados = []
    for i in range(num_dados):
        valor_dado = random.randint(1, tamanho_dado)
        resultados.append(valor_dado)
        print(f"Lançamento n.{i+1} - {valor_dado}")

    print(f"\n{num_dados} dado(s) de {tamanho_dado} lados:")
    for resultado in resultados:
        print(resultado, end=' ')
    print("\n")  # adiciona uma quebra de linha para separar as rolagens de dados

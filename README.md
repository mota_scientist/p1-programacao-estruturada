# Documentação

## Exercício 001

### Passo 1 → Importar a Biblioteca

* Esta Biblioteca será utilizada para obtermos números aleatórios não viciados para utilizarmos nos dados que forem rolados

        import random

### Passo 2 → Criando um Looping Infinito

Cria um looping infinito

    while True:
        tamanho_dado_str = input("Forneça o tamanho do dado que será rolado (ENTER para sair): ")
        if not tamanho_dado_str:
            break
        try:
            tamanho_dado = int(tamanho_dado_str)
            if tamanho_dado <= 0:
                raise ValueError
        except ValueError:
            print("A informação passada não é válida!")
            continue

        num_dados_str = input("Forneça o número de dados que serão rolados (em branco == 1): ")
        if not num_dados_str:
            num_dados = 1
        else:
            try:
                num_dados = int(num_dados_str)
                if num_dados <= 0:
                    raise ValueError
            except ValueError:
                print("A informação passada não é válida!")
                continue
OBS → o **continue** interrompe o fluxo normal de execução do loop e o faz avançar para a próxima iteração.

### Passo 3 → Gerando os valores aleatórios e rolando os dados

        resultados = []
        for i in range(num_dados):
            valor_dado = random.randint(1, tamanho_dado)
            resultados.append(valor_dado)
            print(f"Lançamento n.{i+1} - {valor_dado}")

        print(f"\n{num_dados} dado(s) de {tamanho_dado} lados:")
        for resultado in resultados:
            print(resultado, end=' ')
        print("\n") 
---
## Exercício 002

### Passo 1 → Criar funções a serem utilizadas

    # Função que realiza a soma
    def soma(a, b):
        return a + b

    # Função que realiza a subtração
    def subtracao(a, b):
        return a - b

    # Função que realiza a multiplicação
    def multiplicacao(a, b):
        return a * b

    # Função que realiza a divisão
    def divisao(a, b):
        return a / b

### Passo 2 → Criando condições de excessão para evitar a entrada de tipos primitivos não numéricos

    # Inicializa a calculadora com o valor zero

    resultado = 0
    # Loop infinito da calculadora
    while True:
        # Lê um número
        while True:
            try:
                num1 = float(input("Digite um número: "))
                break
            except ValueError:
                print("Número inválido, por favor digite novamente.")
        
        # Lê uma operação
        operacao = input("Digite a operação (+, -, *, /): ")
        
        # Lê outro número
        while True:
            try:
                num2 = float(input("Digite outro número: "))
                break
            except ValueError:
                print("Número inválido, por favor digite novamente.")
### Passo 3 → Realizar o conta

    # Realiza a operação escolhida
    if operacao == "+":
        resultado = soma(num1, num2)
    elif operacao == "-":
        resultado = subtracao(num1, num2)
    elif operacao == "*":
        resultado = multiplicacao(num1, num2)
    elif operacao == "/":
        resultado = divisao(num1, num2)
    else:
        print("Operação inválida!")
        continue
    
    # Exibe o resultado
    print(f"Resultado: {resultado}")

### Passo 4 → Manter o Looping Infinito zerando a calculadora

    # Pede uma outra operação ou para limpar a calculadora
    opcao = input("Digite a próxima operação ou 'X' para limpar a calculadora: ")
    
    # Limpa a calculadora se o usuário escolher a opção 'X'
    if opcao == "X":
        resultado = 0
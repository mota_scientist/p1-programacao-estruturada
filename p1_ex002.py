# Função que realiza a soma
def soma(a, b):
    return a + b

# Função que realiza a subtração
def subtracao(a, b):
    return a - b

# Função que realiza a multiplicação
def multiplicacao(a, b):
    return a * b

# Função que realiza a divisão
def divisao(a, b):
    return a / b

# Inicializa a calculadora com o valor zero
resultado = 0

# Loop infinito da calculadora
while True:
    # Lê um número
    while True:
        try:
            num1 = float(input("Digite um número: "))
            break
        except ValueError:
            print("Número inválido, por favor digite novamente.")
    
    # Lê uma operação
    operacao = input("Digite a operação (+, -, *, /): ")
    
    # Lê outro número
    while True:
        try:
            num2 = float(input("Digite outro número: "))
            break
        except ValueError:
            print("Número inválido, por favor digite novamente.")
    
    # Realiza a operação escolhida
    if operacao == "+":
        resultado = soma(num1, num2)
    elif operacao == "-":
        resultado = subtracao(num1, num2)
    elif operacao == "*":
        resultado = multiplicacao(num1, num2)
    elif operacao == "/":
        resultado = divisao(num1, num2)
    else:
        print("Operação inválida!")
        continue
    
    # Exibe o resultado
    print(f"Resultado: {resultado}")
    
    # Pede uma outra operação ou para limpar a calculadora
    opcao = input("Digite a próxima operação ou 'X' para limpar a calculadora: ")
    
    # Limpa a calculadora se o usuário escolher a opção 'X'
    if opcao == "X":
        resultado = 0